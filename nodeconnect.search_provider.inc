<?php

/**
 * Standard interface that is used by the nodeconnect search and reference
 * functionality for searching and filtering content.
 * Search providers provide the means for searching and filtering content.
 * They also implement (or proxy) a searching mechanism and return the results.
 * The results are then used to construct a tableselect field.
 */
interface NodeconnectSearchProvider {

  /**
   * Initialize the search provider.
   *
   * @param $field_info
   *   field_info_field() return value for the field we are attaching this search
   *   provider to.
   * @param $init_args
   *   Initialization arguments specified by the search provider info.
   */
  public function __construct(array $field_info, array $init_args);

  /**
   * Returns a rendered form for result filtering.
   */
  public function getFilterForm();

  /**
   * Execute the query.
   */
  public function execute();

  /**
   * Get the results fields in the form of a table header array.
   *
   * $fields = array(
   *  'field_1' => 'Value1',
   *  'field_2' => 'Value2',
   *  ...
   * );
   */
  public function getFields();

  /**
   * Get the results in the form of a table rows array indexed with entity_id.
   *
   * $results = array(
   *  'entity-id-1' => array('field_1' => 'Value1', 'field_2' => 'Value2'),
   *  'entity-id-2' => array('field_1' => 'Value1', 'field_2' => 'Value2'),
   *  ...
   * );
   */
  public function getResults();

  /**
   * Render the results pager (if there is any).
   */
  public function renderPager();
}