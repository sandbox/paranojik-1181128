<?php

/**
 * Base class for view search providers.
 */
abstract class BaseViewSearchProvider implements NodeconnectSearchProvider {

  /**
   * The view
   * @var type view
   */
  protected $view;

  /**
   * Field informationa as provided by field_info_field().
   * @var type array
   */
  protected $field_info;

  public function __construct(array $field_info, array $init_args) {
    $this->field_info = $field_info;
    $this->view = views_get_view($init_args['view_name']);
    $this->view->set_display('default');
    $this->view->override_path = request_path(); // override the path for the exposed form
    $this->view->init_display();
  }

  /**
   * Render the views exposed filters form.
   * @return type
   */
  public function getFilterForm() {
    $this->execute();
    $exposed_form = $this->view->display_handler->get_plugin('exposed_form');
    $exposed_form->pre_render($this->view->result);
    return $exposed_form->render_exposed_form();
  }

  /**
   * Execute the view.
   */
  public function execute() {
    $this->view->pre_execute();
    $this->view->execute();
    $this->view->post_execute();

    // Initialize the style plugin.
    $this->view->init_style();

    // Give field handlers the opportunity to perform additional queries
    // using the entire resultset prior to rendering.
    if ($this->view->style_plugin->uses_fields()) {
      foreach ($this->view->field as $id => $handler) {
        if (!empty($this->view->field[$id])) {
          $this->view->field[$id]->pre_render($this->view->result);
        }
      }
    }

    $this->view->style_plugin->pre_render($this->view->result);
  }

  /**
   * Get the result fields so we can use them as table header.
   * @return type
   */
  public function getFields() {
    $this->execute();
    $header = array();
    // Compose the table header
    foreach ($this->view->field as $key => $field) {
      $header[$field->field_alias] = $field->definition['title'];
    }
    return $header;
  }

  /**
   * Render the views pager.
   * @return type
   */
  public function renderPager() {
    $this->execute();
    $exposed_input = isset($this->view->exposed_data_raw) ? $this->view->exposed_data_raw : NULL;
    $this->view->query->pager->pre_render($this->view->result);
    $output = $this->view->query->render_pager($exposed_input);
    return $output;
  }

  protected function apply_type_filter() {
    // Correctly set the views type filter
    $options = array();
    foreach ($this->field_info['settings']['referenceable_types'] as $type) {
      if ($type != '0') {
        $options[$type] = $type;
      }
    }
    $type_filter = $this->view->get_item('default', 'filter', 'type');

    // Set a new type filter if it doesn't already exist
    if ($type_filter == NULL && !empty($options)) {
      $id = $this->view->add_item('default', 'filter', 'node', 'type', array('value' => $options, 'operator' => 'in'));
      $type_filter = $this->view->get_item('default', 'filter', $id);
    }
    // Reduce the values filter so it covers only referenceable content types
    else if ($type_filter != NULL && !empty($type_filter['value'])) {
      $allowed_values = array_intersect($options, $type_filter['value']);
      $type_filter['value'] = $allowed_values;
    }
    else if ($type_filter != NULL) {
      $type_filter['value'] = $options;
    }

    if ($type_filter != NULL) {
      // Hide this filter if only one value is possible
      if (count($options) == 1) {
        $type_filter['exposed'] = FALSE;
      }
      // If we are limited to some content types and the filter is exposed
      // set the filter to only allow certain values and make it required
      else if (count($options) > 1 && $type_filter['exposed']) {
        $type_filter['expose']['required'] = TRUE;
        $type_filter['expose']['reduce'] = TRUE;
      }
      // Set the modified filter
      $this->view->set_item('default', 'filter', 'type', $type_filter);
    }
  }
}

/**
 * Standard content(node) display view.
 */
class NodeViewSearchProvider extends BaseViewSearchProvider {

  /**
   * Apply node type filtering to the view as needed for the field.
   */
  public function __construct(array $field_info, array $init_args) {
    parent::__construct($field_info, $init_args);
    $this->apply_type_filter();
  }

  /**
   * Return the view results array.
   */
  public function getResults() {
    $this->execute();
    $rows = array();

    if (isset($this->view->style_plugin)) {
      // Let's render the resulting rows
      $this->view->style_plugin->pre_render($this->view->result);
      foreach ($this->view->result as $id => $result) {
        $this->view->row_index = $id;
        foreach ($this->view->field as $key => $field) {
          $row[$field->field_alias] = $field->advanced_render($result);
        }
        $rows[$result->nid] = $row;
      }
    }
    return $rows;
  }
}

/**
 * search_api_views_index queries map to node differently
 *
 * TODO: add default type filter
 */
class SearchApiViewSearchProvider extends BaseViewSearchProvider {

  /**
   * Apply node type filtering to the view as needed for the field.
   */
  public function __construct(array $field_info, array $init_args) {
    parent::__construct($field_info, $init_args);
    $this->apply_type_filter();
  }

  /**
   * Provide the correct mapping between view results and node ids..
   */
  public function getResults() {
    $this->execute();
    $rows = array();

    // Let's render the resulting rows
    if (isset($this->view->style_plugin)) {
      $this->view->style_plugin->pre_render($this->view->result);
      foreach ($this->view->result as $id => $result) {
        $this->view->row_index = $id;
        foreach ($this->view->field as $key => $field) {
          $row[$field->field_alias] = $field->advanced_render($result);
        }
        if (is_object($result->entity)) {
          $rows[$result->entity->nid] = $row;
        }
        else {
          $rows[$result->entity] = $row;
        }
      }
    }
    return $rows;
  }
}