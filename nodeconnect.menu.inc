<?php
/**
 * @file
 * Handles all nodeconnect menu item page callbacks
 */

/*
 * node_connect_return is a hook_menu callback
 * redirects to the form page with the build_cache_id as a get param
 */
function nodeconnect_return($cache_id) {
  $cache = cache_get($cache_id);
  $css_id = "edit-" . str_replace('_', '-', $cache->data['field']);
  drupal_goto($cache->data['dest'], array('query' => array("build_cache_id" => $cache_id, "return" => TRUE), 'fragment' => $css_id));
  return $cache_id;
}

/*
 * nodeconnect_add is hook_menu callback 
 *
 * This page will load the cached form info, and display links to each of the referenceable types
 * If there is only one it will redirect to that page
 *
 * This is mostly a copy and hack up of the node add page
 *
 * This page is directed to from the nodeconnect_add_button_submit
 */
function nodeconnect_add($cache_id) {
  $content = array();
  $cache = cache_get($cache_id);
  $field = field_info_field($cache->data['field']);
  $acceptable_types = $field['settings']['referenceable_types'];
  foreach (system_admin_menu_block(menu_get_item("node/add")) as $key => $item) {
    $type = str_replace("-", '_', str_replace("node/add/", "", $item['path']));
    if (isset($acceptable_types[$type]) && $acceptable_types[$type]) {
      $item['href'] = $item['href'] . "/$cache_id";
      $content[$key] = $item;
    }
  }
  if (sizeof($content) == 1) {
    $item = array_pop($content);
    drupal_goto($item['href']);
  }
  $output = theme('node_add_list', array('content' => $content));
  $output .= l(t('Cancel'), "admin/nodeconnect/return/$cache_id");
  return $output;
}

/*
 * nodeconnect_edit is a hook_menu callback
 *
 * we use this to redirect to a edit form but pass the build_cache_id
 */
function nodeconnect_edit($cache_id) {
  $cache = cache_get($cache_id);
  drupal_goto("node/" . $cache->data['nid'] . "/edit", array('query' => array("build_cache_id" => $cache_id, "child" => TRUE)));
  return $cache_id;
}

/*
 * nodeconnect_edit is a hook_menu callback
 *
 * we use this to redirect to a edit form but pass the build_cache_id
 */
function nodeconnect_search($cache_id) {
  $cache = cache_get($cache_id);

  // Retreive widget settings
  $entity_type = $cache->data['form']['#entity_type'];
  $bundle = $cache->data['form']['#bundle'];
  $field_name = $cache->data['field'];

  $field_info = field_info_field($field_name);
  $info = field_info_instance($entity_type, $field_name, $bundle);
  $widget_settings = $info['widget']['settings'];

  // Load the provider
  $provider_machine_name = $widget_settings['nodeconnect']['advanced_search_provider'];
  $class = nodeconnect_get_provider_info($provider_machine_name);
  if ($class && class_exists($class['class'])) {    
    $provider = new $class['class']($field_info, $class['init args']);
  }
  if (!($provider instanceof NodeconnectSearchProvider)) {
    throw new Exception(t('Nodeconnect search provider with machine name !name specifies illegal provider class !class.', array('!name' => $provider_machine_name, '!class' => $class['class'])));
  }

  $output = $provider->getFilterForm();
  $header = $provider->getFields();
  $rows = $provider->getResults();
  $pager = $provider->renderPager();
  
  // And output a tableselect form
  $form = drupal_get_form('nodeconnect_references_form', $header, $rows, $cache_id, $pager, $field_info['cardinality'] != 1);
  $output .= drupal_render($form);
  $output .= l(t('Cancel'), "admin/nodeconnect/return/$cache_id");

  return $output;
}


/**
 * Transforms view results into a tableselect form with a pager.
 *
 * @param type $form
 * @param type $form_state
 * @param type $header
 *    Table header.
 * @param type $rows
 *    Table rows.
 * @param type $cache_id
 *    The nodeconnect cache_id that needs to be stored.
 * @param type $pager
 *    Data pagination.
 * @param type $select_multiple
 *    Is this a multivalue field.
 * @return string
 *    The renderend form.
 */
function nodeconnect_references_form($form, &$form_state, $header, $rows, $cache_id, $pager, $select_multiple) {
  $form['table'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $rows,
    '#empty' => t('No elements available'),
    '#multiple' => $select_multiple,
    '#js_select' => FALSE,
  );
  
  $form['cache_id'] = array(
    '#type' => 'value',
    '#value' => $cache_id,
  );

  $form['pager'] = array(
    '#markup' => $pager,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('nodeconnect_include_form', 'nodeconnect_search_form_submit'),
  );
  return $form;
}

/**
 * Handles the submission of the search form.
 * Stores the selected nids in the nodeconnect cache and redirects to the
 * origin node_edit form.
 */
function nodeconnect_search_form_submit(&$form, &$form_state) {
  $cache_id =  $form_state['values']['cache_id'];

  if ($cache_id && ($cache = cache_get($cache_id))) {
    $data = $cache->data;

    // Handle multiple values properly
    if (is_array($form_state['values']['table'])) {
      foreach ($form_state['values']['table'] as $row => $nid) {
        if ($nid != 0) {
          $data['nid'][] = $nid;
        }
      }
    }
    // One return value
    else {
      $nid = $form_state['values']['table'];
      $data['nid'] = $nid;
    }
    
    nodeconnect_cache_set($cache_id, $data);
    $form_state['redirect']  = "admin/nodeconnect/return/$cache_id";
  }
}